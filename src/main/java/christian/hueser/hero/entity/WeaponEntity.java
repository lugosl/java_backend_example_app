package christian.hueser.hero.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity(name = "Weapon")
@Table(name = "weapon")
public class WeaponEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="weapon_id")
	private Long weaponId;
	
	@Column(name="weapon_name")
	private String weaponName;
	
	@ManyToMany(mappedBy = "weapons")
	private List<HeroEntity> heros;
	
	public WeaponEntity() {

	}
	
	public WeaponEntity(Long weaponId, String weaponName) {
		this.weaponId = weaponId;
		this.weaponName = weaponName;
	}
	
	public Long getWeaponId() {
		return this.weaponId;
	}
	
	public void setWeaponId(Long weaponId) {
		this.weaponId = weaponId;
	}
	
	public String getWeaponName() {
		return this.weaponName;
	}
	
	public void setWeaponName(String weaponName) {
		this.weaponName = weaponName;
	}
	
}
