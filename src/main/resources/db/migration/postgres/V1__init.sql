CREATE TABLE hero
(
	hero_id integer NOT NULL,
    hero_name VARCHAR(255),
    PRIMARY KEY (hero_id)
);

CREATE TABLE weapon
(
	weapon_id integer NOT NULL,
    weapon_name VARCHAR(255),
    PRIMARY KEY (weapon_id)
);

CREATE TABLE hero_weapon
(
	hero_id integer NOT NULL,
    weapon_id integer NOT NULL
);
