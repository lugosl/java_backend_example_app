package christian.hueser.hero.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import christian.hueser.hero.entity.HeroEntity;
import christian.hueser.hero.repository.IHeroRepository;

@Service
@Transactional
public class HeroService implements IHeroService {
	
   @Autowired
   private IHeroRepository heroRepository;		   
   
   @Override
	public HeroEntity getHeroByHeroId(Long heroId) {
	    HeroEntity obj = heroRepository.getOne(heroId);
		return obj;
	}
   
	@Override
	public List<HeroEntity> getAllHeroes() {
		List<HeroEntity> list = heroRepository.findAll();
		return list;
	}
      
}
